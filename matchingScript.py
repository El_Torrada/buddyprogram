import numpy as np
import random
import math
from simanneal import Annealer


class Matcher(Annealer):
    def __init__(self, state, distance_matrix):
        self.distance_matrix = distance_matrix
        super(Matcher, self).__init__(state)  # important!

    def move(self):
        initial_energy = self.energy()

        a = random.randint(0, len(self.state) - 1)
        b = random.randint(0, len(self.state) - 1)
        self.state[a], self.state[b] = self.state[b], self.state[a]

        return self.energy() - initial_energy

    def energy(self):
        """Calculates the length of the route."""
        e = 0
        for i in range(len(self.state)):
            e += self.distance_matrix[i][self.state[i]]
        return e


def main():
    incomers = parseData('dataIncomers')
    locals = parseData('dataLocals')
    distance_matrix = np.zeros((len(incomers), len(locals)))


    questionCorrespondIncomerToLocal = [[4, 4], [8, 8], [9, 10], [10, 11], [15, 18], [16, 19], [17, 20], [18, 21], [19, 22], [20, 23], [21, 24], [22, 25]]
    weights = [1,1,4,1,1,1,1,1,1,1,1,1]
    genderWeightYes = 2
    genderWeightNo = -10
    languagePreferenceWeight = 1
    incomerGenderPreferenceDict = {"male": 12,
                        "female": 13,
                        "Other (Diverse)": 14}
    localGenderPreferenceDict = {"male": 14,
                        "female": 15,
                        "Other (Diverse)": 16}



    #sample Data
    print("Incomer Example")
    for index, value in enumerate(incomers[0]):
        print(index, value)
    print("-----------------------------------------")
    print("Local Example")
    for index, value in enumerate(locals[0]):
        print(index, value)

    #berechnungen
    for indexIncomer, incomer in enumerate(incomers):
        for indexLocal, local in enumerate(locals):
            for comparison in range(len(questionCorrespondIncomerToLocal)):   #Normal question Comparison
                answersIncomer = incomer[questionCorrespondIncomerToLocal[comparison][0]].split(',')
                answersLocal = local[questionCorrespondIncomerToLocal[comparison][1]].split(',')

                for answer in answersIncomer:
                    if answer in answersLocal:
                        #print("hit", answersIncomer, answersLocal)
                        distance_matrix[indexIncomer][indexLocal]+=(1*weights[comparison])
            #gender
            incomerGender = incomer[5]
            localGender = local[5]

            if local[localGenderPreferenceDict[incomerGender]] == "Yes":
                distance_matrix[indexIncomer][indexLocal]+=(1*genderWeightYes)
            if incomer[incomerGenderPreferenceDict[localGender]] == "Yes":
                distance_matrix[indexIncomer][indexLocal]+=(1*genderWeightYes)
            if local[localGenderPreferenceDict[incomerGender]] == "No":
                distance_matrix[indexIncomer][indexLocal]+=(1*genderWeightNo)
            if incomer[incomerGenderPreferenceDict[localGender]] == "No":
                distance_matrix[indexIncomer][indexLocal]+=(1*genderWeightNo)

            #language should speak compatibility
            incomerShouldSpeak = local[13].split(',')
            incomerSpeaks = incomer[10].split(',') + incomer[11].split('11')
            for language in incomerShouldSpeak:
                if language in incomerSpeaks:
                    distance_matrix[indexIncomer][indexLocal]+=(1*languagePreferenceWeight)

    print('\n\n\n\n\n')
    print(distance_matrix)


    #create Initial State
    init_state = []
    localIndex = 0
    for incomerIndex in range(len(incomers)):
        round = math.floor(incomerIndex/len(locals))
        while int(locals[localIndex][17]) < round:
            localIndex=(localIndex+1)%len(locals)
        init_state.append(localIndex)
        localIndex=(localIndex+1)%len(locals)

    print("this")
    print(init_state)
    tsp = Matcher(init_state, distance_matrix)
    tsp.set_schedule(tsp.auto(minutes=0.2))
    tsp.copy_strategy = "slice"
    state, e = tsp.anneal()
    print("\n\n\n\n")
    print(state, e)

def parseData(file):
    returnArray = []
    with open(file + '.txt') as f:
        for line in f.readlines() :
            returnArray.append(line.lower().strip().split('\t'))
    return returnArray

main()
